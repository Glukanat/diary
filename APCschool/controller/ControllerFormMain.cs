﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using APCschool.model;

namespace APCschool.controller
{
    class ControllerFormMain
    {
        private DBprovider dBprovider;
        private FormAIS formAIS;

        public ControllerFormMain(FormAIS fAIS)
        {
            formAIS = fAIS;
            try
            {
                dBprovider = DBprovider.GetInstance();
            }
            catch
            {
                Application.Exit();
            }
        }
        #region tablestudents
        public void FillDGVStudents()
        {
            formAIS.dataGridViewStudents.DataSource = dBprovider.TableStudents.Rows;
            formAIS.dataGridViewStudents.Columns["Id"].Visible = false;

        }
        public void AddStudent()
        {
            string name = formAIS.textBoxStudentName.Text;
            int age = (int)formAIS.numericUpDownStudentAge.Value;
            string group = formAIS.textBoxStudentGroup.Text;
            if (name == String.Empty || group == String.Empty)
            {
                MessageBox.Show("error");
                return;
            }
            else
            {
                Student student = new Student()
                {
                    Name = name,
                    Age = age,
                    Group = group


                };

                if (dBprovider.TableStudents.Add(student))
                {
                    MessageBox.Show("sucsess add");
                    ClearStudentsFilds();
                }
                else
                {
                    MessageBox.Show("error");
                }
            }
        }
        public void ClearStudentsFilds()
        {
            formAIS.textBoxStudentName.Clear();
            formAIS.numericUpDownStudentAge.Value = 7;
            formAIS.textBoxStudentGroup.Clear();
            formAIS.textBoxStudentID.Clear();
        }
        public void FillSelectedStudentFields()
        {
            if (formAIS.dataGridViewStudents.SelectedRows.Count != 0)
            {
                DataGridViewRow row = formAIS.dataGridViewStudents.SelectedRows[0];

                formAIS.textBoxStudentID.Text = row.Cells["Id"].Value.ToString();
                formAIS.textBoxStudentName.Text = row.Cells["Name"].Value.ToString();
                formAIS.textBoxStudentGroup.Text = row.Cells["Group"].Value.ToString();
                formAIS.numericUpDownStudentAge.Value = decimal.Parse(row.Cells["Age"].Value.ToString());


            }
        }
        public void DeleteStudent()
        {
            string id = formAIS.textBoxStudentID.Text;
            if (id == String.Empty)
            {
                MessageBox.Show("erore");
            }
            else
            {
                if (dBprovider.TableStudents.Delete(int.Parse(id)))
                {
                    MessageBox.Show("sucsess delete");
                    ClearStudentsFilds();
                }
                else
                {
                    MessageBox.Show("errore at deleting");
                }

            }

        }
        public void UpdateStudent()
        {
            string name = formAIS.textBoxStudentName.Text;
            int age = (int)formAIS.numericUpDownStudentAge.Value;
            string group = formAIS.textBoxStudentGroup.Text;
            string id = formAIS.textBoxStudentID.Text;
            if (name == String.Empty || group == String.Empty || id == String.Empty)
            {
                MessageBox.Show("error");
                return;
            }
            else
            {
                Student student = new Student()
                {
                    Name = name,
                    Age = age,
                    Group = group,
                    Id = int.Parse(id)


                };

                if (dBprovider.TableStudents.Update(student))
                {
                    MessageBox.Show("Sucsess update");
                    ClearStudentsFilds();
                    formAIS.dataGridViewStudents.Refresh();
                    
                }
                else
                {
                    MessageBox.Show("Error");
                }
            }
        }
        #endregion
        #region tablemarks
        public void FillDGVSubjects()
        {
            formAIS.dataGridViewMarks.DataSource = dBprovider.TableMarks.Rows;
            formAIS.dataGridViewMarks.Columns["DateTime"].DefaultCellStyle.Format = "dd.MM.yyyy H:mm:ss";
        }
        public void FillComboboxMarks()
        {
            formAIS.comboBoxMarkSubject.DataSource = dBprovider.TableSubjects.Rows;


        }
        public void FillComboboxStudents()
        {
            formAIS.comboBoxMarkStudent.DataSource = null;
            formAIS.comboBoxMarkStudent.DataSource = dBprovider.TableStudents.Rows;
        }
        public void FillSelectedMarkFields()
        {
            if (formAIS.dataGridViewMarks.SelectedRows.Count != 0)
            {
                int selectedIndex = formAIS.dataGridViewMarks.SelectedRows[0].Index;
                Mark selectedMark = dBprovider.TableMarks.Rows[selectedIndex];
                formAIS.dateTimePickerMark.Value = selectedMark.DateTime;
                for (int i = 0; i < formAIS.comboBoxMarkStudent.Items.Count; i++)
                {
                    if (((Student)formAIS.comboBoxMarkStudent.Items[i]).Id == selectedMark.Student.Id)
                    {
                        formAIS.comboBoxMarkStudent.SelectedIndex = i;
                        break;
                    }
                }
                for (int i = 0; i < formAIS.comboBoxMarkSubject.Items.Count; i++)
                {
                    if (((Subject)formAIS.comboBoxMarkSubject.Items[i]).Id == selectedMark.Subject.Id)
                    {
                        formAIS.comboBoxMarkSubject.SelectedIndex = i;
                        break;
                    }
                }
                formAIS.numericUpDownMarkGrade.Value = selectedMark.Grade;
                formAIS.textBoxMarkId.Text = selectedMark.Id.ToString();
            }
        }
        public void FillTableMarks()
        {
            formAIS.dataGridViewMarks.DataSource = dBprovider.TableMarks.Rows;
            formAIS.dataGridViewMarks.Columns["Id"].Visible = false;
        }
        public void FillComboBoxSearchStudent()
        {
            formAIS.comboBoxSearchStudentMarks.DataSource = null;
            formAIS.comboBoxSearchStudentMarks.DataSource = dBprovider.TableStudents.Rows;
        }
        public void AddMark()
        {
            DateTime dateTime = formAIS.dateTimePickerMark.Value;
            Student student = (Student)formAIS.comboBoxMarkStudent.SelectedItem;
            Subject subject = (Subject)formAIS.comboBoxMarkSubject.SelectedItem;
            int grade = (int)formAIS.numericUpDownMarkGrade.Value;
            Mark mark = new Mark()
            {

                DateTime = dateTime,
                Student = student,
                Subject = subject,
                Grade = grade

            };
            if (dBprovider.TableMarks.Add(mark))
            {
                MessageBox.Show("Sucsess add");
            }
            else
            {
                MessageBox.Show("error at addating");
            }
        }
        public void DeleteMark()
        {
            if (formAIS.textBoxMarkId.Text == String.Empty)
            {
                MessageBox.Show("error,chose row");
                return;
            }
            int id = int.Parse(formAIS.textBoxMarkId.Text);
            if (dBprovider.TableMarks.Delete(id))
            {
                MessageBox.Show("Sucsess delete");
            }
            else
            {
                MessageBox.Show("error at deleting");
            }
        }
        public void UpdateMark()
        {
            Mark mark = new Mark()
            {
                Id = int.Parse(formAIS.textBoxMarkId.Text),
                DateTime = formAIS.dateTimePickerMark.Value,
                Student = (Student)formAIS.comboBoxMarkStudent.SelectedItem,
                Subject = (Subject)formAIS.comboBoxMarkSubject.SelectedItem,
                Grade = (int)formAIS.numericUpDownMarkGrade.Value
            };
            if (dBprovider.TableMarks.Update(mark))
            {
                MessageBox.Show("Sucsess update");

                formAIS.dataGridViewMarks.Refresh();
            }
            else
            {
                MessageBox.Show("Error at updating");
            }
        }
        public void SearchMarkAStudent()
        {
           
        }
        #endregion

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace APCschool.model
{
    class DBConnectior
    {
        private MySqlCommand _command;
        private MySqlConnection _connection;
        private static DBConnectior _instance = null;

        private DBConnectior()
        {
            string connectionStrint = "Server=localhost;Port=3306;Database=apcshool;User=root;Password=1234;";
            _connection = new MySqlConnection(connectionStrint);
            _connection.Open();

            _command = new MySqlCommand();
            _command.Connection = _connection;
        }

        public static DBConnectior GetInstence()
        {
            if (_instance == null)
            {
                _instance = new DBConnectior();
            }
            return _instance;
        }
        public MySqlCommand GetMySqlCommand()
        {
            return _command;
        }
    }
}

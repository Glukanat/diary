﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APCschool.model
{
    class DBprovider
    {
        private static DBprovider inctance = null;
        public TableSubjects TableSubjects { get; private set; }
        public TableStudents TableStudents { get; private set; }
        public TableMarks TableMarks { get; private set; }

        private DBprovider()
        {
            TableSubjects = new TableSubjects();
            TableStudents = new TableStudents();
            TableMarks = new TableMarks(TableStudents, TableSubjects);
        }
        public static DBprovider GetInstance()
        {
            if (inctance == null)
            {
                inctance = new DBprovider();
            }
            return inctance;
        }
    }
}

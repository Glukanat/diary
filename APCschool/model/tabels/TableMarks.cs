﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.ComponentModel;
namespace APCschool.model
{
    class TableMarks
    {
        private MySqlCommand _command;
        public BindingList<Mark> Rows { get; private set; }

        public TableMarks(TableStudents tableStudents, TableSubjects tableSubjects)
        {
            Rows = new BindingList<Mark>();
            _command = DBConnectior.GetInstence().GetMySqlCommand();
            _command.CommandText = "CALL marks_select_all()";

            MySqlDataReader dataReader = _command.ExecuteReader();
            while (dataReader.Read())
            {
                int id = dataReader.GetInt32("id");
                DateTime dateTime = dataReader.GetDateTime("datetime");
                int id_student = dataReader.GetInt32("id_student");
                int id_subject = dataReader.GetInt32("id_subject");
                int grade = dataReader.GetInt32("grade");

                Student student = null;
                Subject subject = null;

                for (int i = 0; i < tableStudents.Rows.Count; i++)
                {
                    if (tableStudents.Rows[i].Id == id_student)
                    {
                        student = tableStudents.Rows[i];
                        break;
                    }
                }
                for (int i = 0; i < tableSubjects.Rows.Count; i++)
                {
                    if (tableStudents.Rows[i].Id == id_subject)
                    {
                        subject = tableSubjects.Rows[i];
                        break;
                    }

                }
                Rows.Add(new Mark()
                {
                    Id = id,
                    DateTime = dateTime,
                    Student = student,
                    Subject = subject,
                    Grade = grade
                });
            }
            dataReader.Close();

        }
        
        public bool Add(Mark mark)
        {
            try
            {
                _command.CommandText = $@"CALL marks_insert(
                '{mark.DateTime.ToString("yyyy-MM-dd H:mm:ss")}',
                {mark.Student.Id},
                {mark.Subject.Id},
                {mark.Grade}
                )";
                _command.ExecuteNonQuery();
                _command.CommandText = $"CALL get_last_insert_id()";
                mark.Id = int.Parse(_command.ExecuteScalar().ToString());

                Rows.Add(mark);


                return true;
            }
            catch
            {
                return false;
            }


        }

        public bool Delete(int id)
        {
            try
            {
                _command.CommandText = $"CALL marks_delete_byid({id})";
                _command.ExecuteNonQuery();
                for (int i = 0; i < Rows.Count; i++)
                {
                    if (Rows[i].Id == id)
                    {
                        Rows.RemoveAt(i);
                        break;
                    }
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Search()
        {
            try
            {
                //for (int i = 0; i < Rows.Count; i++)
                //{
                //    if(Rows[i].Student == )
                //}
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Update(Mark mark)
        {
            try
            {
                _command.CommandText = $@"CALL marks_update_byid(
                {mark.Id},
                '{mark.DateTime.ToString("yyyy-MM-dd H:mm:ss")}',
                {mark.Student.Id},
                {mark.Subject.Id},
                {mark.Grade}
                )";
                _command.ExecuteNonQuery();
                for (int i = 0; i < Rows.Count; i++)
                {
                    if (Rows[i].Id == mark.Id)
                    {
                        Rows[i].DateTime = mark.DateTime;
                        Rows[i].Student = mark.Student;
                        Rows[i].Subject = mark.Subject;
                        Rows[i].Grade = mark.Grade;
                        break;
                    }
                }

                return true;
            }
            catch
            {
                
                return false;
            }
        }
    }
}

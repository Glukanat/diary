﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.ComponentModel;

namespace APCschool.model
{
    class TableStudents
    {
        private MySqlCommand _command;
        public BindingList<Student> Rows { get; private set; }

        public TableStudents()
        {
            _command = DBConnectior.GetInstence().GetMySqlCommand();
            LoadRows();
        }
        private void LoadRows()
        {
            Rows = new BindingList<Student>();
            _command.CommandText = "CALL students_select_all()";

            MySqlDataReader dataReader = _command.ExecuteReader();
            while (dataReader.Read())
            {
                Rows.Add(new Student() { Id = dataReader.GetInt32("id"), Name = dataReader.GetString("name"), Age = dataReader.GetInt32("age"), Group = dataReader.GetString("group") });
            }
            dataReader.Close();
        }

        public bool Add(Student student)
        {
            try
            {
                _command.CommandText = $"CALL students_insert('{student.Name}','{student.Group}',{student.Age})";
                _command.ExecuteNonQuery();
                _command.CommandText = "CALL get_last_insert_id()";
                student.Id = int.Parse(_command.ExecuteScalar().ToString());
                Rows.Add(student);
                return true;

            }
            catch
            {
                return false;
            }
        }
       
        public bool Delete(int id)
        {
            try
            {
                _command.CommandText = $"CALL students_delete_byid({id})";
                _command.ExecuteNonQuery();
                for (int i = 0; i < Rows.Count; i++)
                {
                    if (Rows[i].Id == id)
                    {
                        Rows.RemoveAt(i);
                        break;
                    }
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool Update(Student student)
        {
            try
            {
                _command.CommandText = $"CALL students_update_byid({student.Id},'{student.Name}','{student.Group}',{student.Age})";
                _command.ExecuteNonQuery();
                for (int i = 0; i < Rows.Count; i++)
                {
                    if(Rows[i].Id== student.Id)
                    {
                        Rows[i].Name = student.Name;
                        Rows[i].Group = student.Group;
                        Rows[i].Age = student.Age;
                        
                        break;
                    }
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
            
    }
}

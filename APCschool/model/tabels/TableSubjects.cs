﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.ComponentModel;
namespace APCschool.model
{
    class TableSubjects
    {
        private MySqlCommand _command;
        public BindingList<Subject> Rows { get; private set; }

        public TableSubjects()
        {
            _command = DBConnectior.GetInstence().GetMySqlCommand();
            LoadRows();

        }
        private void LoadRows()
        {
            Rows = new BindingList<Subject>();
            _command.CommandText = "CALL subject_select_all()";
            MySqlDataReader dataReader = _command.ExecuteReader();
            while (dataReader.Read())
            {
                Rows.Add(new Subject() { Id = dataReader.GetInt32("id"), Name = dataReader.GetString("nume") });
            }
            dataReader.Close();
        }

    }
}

﻿namespace APCschool
{
    partial class FormAIS
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.StudentsTapPage = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.updateStudent = new System.Windows.Forms.Button();
            this.textBoxStudentID = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxStudentName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxStudentGroup = new System.Windows.Forms.TextBox();
            this.addStudent = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.numericUpDownStudentAge = new System.Windows.Forms.NumericUpDown();
            this.deleteStudent = new System.Windows.Forms.Button();
            this.dataGridViewStudents = new System.Windows.Forms.DataGridView();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.dateTimePickerMark = new System.Windows.Forms.DateTimePicker();
            this.textBoxMarkId = new System.Windows.Forms.TextBox();
            this.buttonUpdate = new System.Windows.Forms.Button();
            this.comboBoxMarkStudent = new System.Windows.Forms.ComboBox();
            this.buttonDelete = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.buttonMarkAdd = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.comboBoxMarkSubject = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.numericUpDownMarkGrade = new System.Windows.Forms.NumericUpDown();
            this.comboBoxSearchStudentMarks = new System.Windows.Forms.ComboBox();
            this.dataGridViewMarks = new System.Windows.Forms.DataGridView();
            this.StudentsTapPage.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownStudentAge)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewStudents)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMarkGrade)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewMarks)).BeginInit();
            this.SuspendLayout();
            // 
            // StudentsTapPage
            // 
            this.StudentsTapPage.Controls.Add(this.tabPage1);
            this.StudentsTapPage.Controls.Add(this.tabPage2);
            this.StudentsTapPage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.StudentsTapPage.Location = new System.Drawing.Point(0, 0);
            this.StudentsTapPage.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.StudentsTapPage.Name = "StudentsTapPage";
            this.StudentsTapPage.SelectedIndex = 0;
            this.StudentsTapPage.Size = new System.Drawing.Size(1131, 648);
            this.StudentsTapPage.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Controls.Add(this.dataGridViewStudents);
            this.tabPage1.Location = new System.Drawing.Point(4, 29);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabPage1.Size = new System.Drawing.Size(1123, 615);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "students";
            this.tabPage1.UseVisualStyleBackColor = true;
            this.tabPage1.Click += new System.EventHandler(this.tabPage1_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.updateStudent);
            this.groupBox1.Controls.Add(this.textBoxStudentID);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.textBoxStudentName);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.textBoxStudentGroup);
            this.groupBox1.Controls.Add(this.addStudent);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.numericUpDownStudentAge);
            this.groupBox1.Controls.Add(this.deleteStudent);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(4, 422);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox1.Size = new System.Drawing.Size(1115, 194);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "groupBox1";
            // 
            // updateStudent
            // 
            this.updateStudent.Location = new System.Drawing.Point(254, 26);
            this.updateStudent.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.updateStudent.Name = "updateStudent";
            this.updateStudent.Size = new System.Drawing.Size(112, 35);
            this.updateStudent.TabIndex = 6;
            this.updateStudent.Text = "update";
            this.updateStudent.UseVisualStyleBackColor = true;
            this.updateStudent.Click += new System.EventHandler(this.updateStudent_Click);
            // 
            // textBoxStudentID
            // 
            this.textBoxStudentID.Location = new System.Drawing.Point(1046, 26);
            this.textBoxStudentID.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textBoxStudentID.Name = "textBoxStudentID";
            this.textBoxStudentID.Size = new System.Drawing.Size(40, 26);
            this.textBoxStudentID.TabIndex = 4;
            this.textBoxStudentID.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(531, 66);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(50, 20);
            this.label3.TabIndex = 10;
            this.label3.Text = "group";
            // 
            // textBoxStudentName
            // 
            this.textBoxStudentName.Location = new System.Drawing.Point(14, 111);
            this.textBoxStudentName.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textBoxStudentName.Name = "textBoxStudentName";
            this.textBoxStudentName.Size = new System.Drawing.Size(148, 26);
            this.textBoxStudentName.TabIndex = 1;
            this.textBoxStudentName.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(234, 66);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(36, 20);
            this.label2.TabIndex = 9;
            this.label2.Text = "age";
            // 
            // textBoxStudentGroup
            // 
            this.textBoxStudentGroup.Location = new System.Drawing.Point(536, 109);
            this.textBoxStudentGroup.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textBoxStudentGroup.Name = "textBoxStudentGroup";
            this.textBoxStudentGroup.Size = new System.Drawing.Size(148, 26);
            this.textBoxStudentGroup.TabIndex = 2;
            // 
            // addStudent
            // 
            this.addStudent.Location = new System.Drawing.Point(14, 29);
            this.addStudent.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.addStudent.Name = "addStudent";
            this.addStudent.Size = new System.Drawing.Size(112, 35);
            this.addStudent.TabIndex = 5;
            this.addStudent.Text = "add";
            this.addStudent.UseVisualStyleBackColor = true;
            this.addStudent.Click += new System.EventHandler(this.addStudent_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 69);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 20);
            this.label1.TabIndex = 8;
            this.label1.Text = "name";
            // 
            // numericUpDownStudentAge
            // 
            this.numericUpDownStudentAge.Location = new System.Drawing.Point(238, 111);
            this.numericUpDownStudentAge.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.numericUpDownStudentAge.Name = "numericUpDownStudentAge";
            this.numericUpDownStudentAge.Size = new System.Drawing.Size(180, 26);
            this.numericUpDownStudentAge.TabIndex = 3;
            // 
            // deleteStudent
            // 
            this.deleteStudent.Location = new System.Drawing.Point(536, 26);
            this.deleteStudent.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.deleteStudent.Name = "deleteStudent";
            this.deleteStudent.Size = new System.Drawing.Size(112, 35);
            this.deleteStudent.TabIndex = 7;
            this.deleteStudent.Text = "delete";
            this.deleteStudent.UseVisualStyleBackColor = true;
            this.deleteStudent.Click += new System.EventHandler(this.deleteStudent_Click);
            // 
            // dataGridViewStudents
            // 
            this.dataGridViewStudents.AllowUserToAddRows = false;
            this.dataGridViewStudents.AllowUserToDeleteRows = false;
            this.dataGridViewStudents.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewStudents.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewStudents.Dock = System.Windows.Forms.DockStyle.Top;
            this.dataGridViewStudents.Location = new System.Drawing.Point(4, 5);
            this.dataGridViewStudents.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.dataGridViewStudents.Name = "dataGridViewStudents";
            this.dataGridViewStudents.ReadOnly = true;
            this.dataGridViewStudents.Size = new System.Drawing.Size(1115, 417);
            this.dataGridViewStudents.TabIndex = 0;
            this.dataGridViewStudents.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewStudents_CellClick);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBox2);
            this.tabPage2.Controls.Add(this.comboBoxSearchStudentMarks);
            this.tabPage2.Controls.Add(this.dataGridViewMarks);
            this.tabPage2.Location = new System.Drawing.Point(4, 29);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabPage2.Size = new System.Drawing.Size(1123, 615);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "diary";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.LightGray;
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.dateTimePickerMark);
            this.groupBox2.Controls.Add(this.textBoxMarkId);
            this.groupBox2.Controls.Add(this.buttonUpdate);
            this.groupBox2.Controls.Add(this.comboBoxMarkStudent);
            this.groupBox2.Controls.Add(this.buttonDelete);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.buttonMarkAdd);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.comboBoxMarkSubject);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.numericUpDownMarkGrade);
            this.groupBox2.Location = new System.Drawing.Point(19, 280);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox2.Size = new System.Drawing.Size(1095, 168);
            this.groupBox2.TabIndex = 14;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "WorkWithMarks";
            this.groupBox2.Enter += new System.EventHandler(this.groupBox2_Enter);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 34);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(43, 20);
            this.label4.TabIndex = 7;
            this.label4.Text = "Time";
            // 
            // dateTimePickerMark
            // 
            this.dateTimePickerMark.CustomFormat = "dd.MM.yyyy H:mm:ss";
            this.dateTimePickerMark.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerMark.Location = new System.Drawing.Point(14, 58);
            this.dateTimePickerMark.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.dateTimePickerMark.Name = "dateTimePickerMark";
            this.dateTimePickerMark.Size = new System.Drawing.Size(298, 26);
            this.dateTimePickerMark.TabIndex = 1;
            // 
            // textBoxMarkId
            // 
            this.textBoxMarkId.Location = new System.Drawing.Point(884, 128);
            this.textBoxMarkId.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textBoxMarkId.Name = "textBoxMarkId";
            this.textBoxMarkId.Size = new System.Drawing.Size(32, 26);
            this.textBoxMarkId.TabIndex = 5;
            // 
            // buttonUpdate
            // 
            this.buttonUpdate.Location = new System.Drawing.Point(302, 100);
            this.buttonUpdate.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.buttonUpdate.Name = "buttonUpdate";
            this.buttonUpdate.Size = new System.Drawing.Size(112, 35);
            this.buttonUpdate.TabIndex = 12;
            this.buttonUpdate.Text = "UpdateMark";
            this.buttonUpdate.UseVisualStyleBackColor = true;
            this.buttonUpdate.Click += new System.EventHandler(this.buttonUpdate_Click);
            // 
            // comboBoxMarkStudent
            // 
            this.comboBoxMarkStudent.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxMarkStudent.FormattingEnabled = true;
            this.comboBoxMarkStudent.Location = new System.Drawing.Point(369, 57);
            this.comboBoxMarkStudent.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.comboBoxMarkStudent.Name = "comboBoxMarkStudent";
            this.comboBoxMarkStudent.Size = new System.Drawing.Size(180, 28);
            this.comboBoxMarkStudent.TabIndex = 2;
            // 
            // buttonDelete
            // 
            this.buttonDelete.Location = new System.Drawing.Point(154, 100);
            this.buttonDelete.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.buttonDelete.Name = "buttonDelete";
            this.buttonDelete.Size = new System.Drawing.Size(112, 35);
            this.buttonDelete.TabIndex = 11;
            this.buttonDelete.Text = "DeleteMark";
            this.buttonDelete.UseVisualStyleBackColor = true;
            this.buttonDelete.Click += new System.EventHandler(this.buttonDelete_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(364, 32);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(66, 20);
            this.label5.TabIndex = 8;
            this.label5.Text = "Student";
            // 
            // buttonMarkAdd
            // 
            this.buttonMarkAdd.Location = new System.Drawing.Point(14, 100);
            this.buttonMarkAdd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.buttonMarkAdd.Name = "buttonMarkAdd";
            this.buttonMarkAdd.Size = new System.Drawing.Size(112, 35);
            this.buttonMarkAdd.TabIndex = 6;
            this.buttonMarkAdd.Text = "ADDMark";
            this.buttonMarkAdd.UseVisualStyleBackColor = true;
            this.buttonMarkAdd.Click += new System.EventHandler(this.button1_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(759, 25);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(54, 20);
            this.label7.TabIndex = 10;
            this.label7.Text = "Grade";
            // 
            // comboBoxMarkSubject
            // 
            this.comboBoxMarkSubject.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxMarkSubject.FormattingEnabled = true;
            this.comboBoxMarkSubject.Location = new System.Drawing.Point(573, 57);
            this.comboBoxMarkSubject.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.comboBoxMarkSubject.Name = "comboBoxMarkSubject";
            this.comboBoxMarkSubject.Size = new System.Drawing.Size(180, 28);
            this.comboBoxMarkSubject.TabIndex = 3;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(568, 32);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(48, 20);
            this.label6.TabIndex = 9;
            this.label6.Text = "Class";
            // 
            // numericUpDownMarkGrade
            // 
            this.numericUpDownMarkGrade.Location = new System.Drawing.Point(764, 58);
            this.numericUpDownMarkGrade.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.numericUpDownMarkGrade.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numericUpDownMarkGrade.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.numericUpDownMarkGrade.Name = "numericUpDownMarkGrade";
            this.numericUpDownMarkGrade.Size = new System.Drawing.Size(180, 26);
            this.numericUpDownMarkGrade.TabIndex = 4;
            this.numericUpDownMarkGrade.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // comboBoxSearchStudentMarks
            // 
            this.comboBoxSearchStudentMarks.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxSearchStudentMarks.FormattingEnabled = true;
            this.comboBoxSearchStudentMarks.Location = new System.Drawing.Point(344, 280);
            this.comboBoxSearchStudentMarks.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.comboBoxSearchStudentMarks.Name = "comboBoxSearchStudentMarks";
            this.comboBoxSearchStudentMarks.Size = new System.Drawing.Size(180, 28);
            this.comboBoxSearchStudentMarks.TabIndex = 13;
            this.comboBoxSearchStudentMarks.SelectedIndexChanged += new System.EventHandler(this.comboBoxSearchStudentMarks_SelectedIndexChanged);
            // 
            // dataGridViewMarks
            // 
            this.dataGridViewMarks.AllowUserToAddRows = false;
            this.dataGridViewMarks.AllowUserToDeleteRows = false;
            this.dataGridViewMarks.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewMarks.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewMarks.Dock = System.Windows.Forms.DockStyle.Top;
            this.dataGridViewMarks.Location = new System.Drawing.Point(4, 5);
            this.dataGridViewMarks.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.dataGridViewMarks.Name = "dataGridViewMarks";
            this.dataGridViewMarks.ReadOnly = true;
            this.dataGridViewMarks.Size = new System.Drawing.Size(1115, 265);
            this.dataGridViewMarks.TabIndex = 0;
            this.dataGridViewMarks.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewMarks_CellClick);
            // 
            // FormAIS
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1131, 648);
            this.Controls.Add(this.StudentsTapPage);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "FormAIS";
            this.Text = "AIS";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.StudentsTapPage.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownStudentAge)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewStudents)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMarkGrade)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewMarks)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl StudentsTapPage;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        public System.Windows.Forms.DataGridView dataGridViewStudents;
        public System.Windows.Forms.TextBox textBoxStudentID;
        public System.Windows.Forms.Button deleteStudent;
        public System.Windows.Forms.TextBox textBoxStudentGroup;
        public System.Windows.Forms.Button updateStudent;
        public System.Windows.Forms.NumericUpDown numericUpDownStudentAge;
        public System.Windows.Forms.Button addStudent;
        public System.Windows.Forms.TextBox textBoxStudentName;
        public System.Windows.Forms.DataGridView dataGridViewMarks;
        private System.Windows.Forms.GroupBox groupBox1;
        public System.Windows.Forms.Button buttonMarkAdd;
        public System.Windows.Forms.TextBox textBoxMarkId;
        public System.Windows.Forms.NumericUpDown numericUpDownMarkGrade;
        public System.Windows.Forms.ComboBox comboBoxMarkSubject;
        public System.Windows.Forms.ComboBox comboBoxMarkStudent;
        public System.Windows.Forms.DateTimePicker dateTimePickerMark;
        private System.Windows.Forms.Button buttonDelete;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button buttonUpdate;
        private System.Windows.Forms.GroupBox groupBox2;
        public System.Windows.Forms.ComboBox comboBoxSearchStudentMarks;
    }
}


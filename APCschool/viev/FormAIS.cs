﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using APCschool.controller;

namespace APCschool
{
    public partial class FormAIS : Form
    {
        private ControllerFormMain controller;

        public FormAIS()
        {
            InitializeComponent();
        }


        private void tabPage1_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            controller = new ControllerFormMain(this);
            controller.FillDGVStudents();
            controller.FillDGVSubjects();

            controller.FillComboboxMarks();
            controller.FillComboboxStudents();
            controller.FillComboBoxSearchStudent();

            controller.FillTableMarks();
        }

        private void addStudent_Click(object sender, EventArgs e)
        {
            controller.AddStudent();
        }

        private void dataGridViewStudents_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            controller.FillSelectedStudentFields();
        }

        private void deleteStudent_Click(object sender, EventArgs e)
        {
            controller.DeleteStudent();
        }

        private void updateStudent_Click(object sender, EventArgs e)
        {
            controller.UpdateStudent();
            controller.FillComboboxStudents();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            controller.AddMark();
        }

        private void dataGridViewMarks_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            controller.FillSelectedMarkFields();
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            controller.DeleteMark();
        }

        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            controller.UpdateMark();
        }

        private void comboBoxSearchStudentMarks_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }
    }
}
